﻿using System;
using System.ServiceProcess;

namespace Lighting_Service_Killer
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceController service = new ServiceController("LightingService");

            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(10000);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch
            {
                Console.WriteLine("Failed to stop service...");
                Console.ReadKey();
            }
        }
    }
}
